import React from "react";
import { Route, Switch } from "react-router-dom";
import Connect from "../components/functional/Connect";
import Chat from "../components/class/Chat"

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <Connect />
      </Route>
      <Route exact path="/chat/:username">
        <Chat />
      </Route>
    </Switch>
  );
}
