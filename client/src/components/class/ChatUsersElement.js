import React, { Component } from 'react'

class ChatUsersElement extends Component {
  render() {
    return (
        <div>
          <ul>
            {this.props.users.map(function(item) {
              return <li key={item}>{item}</li>;
            })}
          </ul>
        </div>
    )
  }
}

export default ChatUsersElement