import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Button, FormControl, InputGroup } from "react-bootstrap";


class ChatInputElement extends Component {
  static propTypes = {
    onSubmitMessage: PropTypes.func.isRequired,
  }
  state = {
    message: '',
  }

  validateForm() {
    return this.state.message.length > 0;
  }

  render() {
    return (
      <form
        action="."
        onSubmit={e => {
          e.preventDefault()
          this.props.onSubmitMessage(this.state.message)
          this.setState({ message: '' })
        }}
      >
        <InputGroup className="flex">
          <InputGroup.Prepend>
            <InputGroup.Text>{this.props.username}</InputGroup.Text>
          </InputGroup.Prepend>
          <span className="spanspace"></span>
            <FormControl
              autoFocus
              type="text"
              placeholder="Enter message..."
              value={this.state.message}
              onChange={e => this.setState({ message: e.target.value })}
            />
            <Button disabled={!this.validateForm()} type="submit">
              Send
            </Button>
        </InputGroup>
      </form>
    )
  }
}

export default ChatInputElement