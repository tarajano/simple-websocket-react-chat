import React, { Component } from 'react'
import ChatInputElement from './ChatInputElement'
import ChatUsersElement from './ChatUsersElement'
import ChatMessageElement from './ChatMessageElement'
import ChatDisconnectElement from './ChatDisconnectElement'
import { withRouter } from 'react-router-dom'
import './Chat.css'


class Chat extends Component {
  constructor(props) {
    super();

    this.state = {
        ws: null,
        username: '',
        messages: [],
        connectedUsers : []
    };
  }

  componentDidMount() {
    this.connect();
  }
  
  // connect function that establish a connection and create event listeners for the websocket events.
  connect = () => {
    const username = this.props.match.params.username;
    this.setState({ username: username })
    var ws = new WebSocket(`ws://localhost:8080/chat/${username}`);
    
    ws.onopen = () => {
        this.setState({ ws: ws });
    };

    ws.onmessage = evt => {
      const data = JSON.parse(evt.data)
      if (data.type === "message") {
        this.addMessage(data.source, data.content) 
      } else if (data.type === "userslist") {
        this.updateUsersConnectedList(data.users)
      }
    }

    ws.onerror = err => {
        this.disconnect()
    };
  };

  // addMessage adds a received source and message to the list of messages of the client.
  addMessage = ( source, message ) => {
    this.setState(state => ({ messages: [[source, message], ...state.messages] }))
  }

  // updateUsersConnectedList updates the list of users connected to the application.
  updateUsersConnectedList = users => {
    this.setState(state => ({ connectedUsers: users }))
  }

  // submitMessage sends a message originated in the client side through the websocket
  submitMessage = messageString => {
    const message = { source:this.state.username, type:"message", content: messageString }
    this.state.ws.send(JSON.stringify(message))
  }

  // disconnect closes the websocket and redirects the application to the connect page
  disconnect = () => {
    if (this.state.ws != null){
      this.state.ws.close()
    } 
    this.props.history.push(`/`);
  }

  render() {
    return (
      <div>
        <div id="MessagesUsersContainer" className="flexContainer">
          <div id="Messages" className="messagesContainer">
            {this.state.messages.map((message, index) =>
              <ChatMessageElement
                key={index}
                message={message}
                name={message.username}
              />,
            )}
          </div>
          <div id="ConnectedUsers" className="usersContainer">
            <ChatUsersElement
                users={this.state.connectedUsers}
            />
          </div>
        </div>
        <div id="SubmitDisconnectContainer" className="flexContainer">
          <div id="SubmitForm" className="submitContainer">
            <ChatInputElement
              ws={this.state.ws}
              username={this.state.username}
              onSubmitMessage={messageString => this.submitMessage(messageString)}
            />
          </div>
          <div id="DisconnectForm" className="disconnectContainer">
            <ChatDisconnectElement
              onDisconnect={() => this.disconnect()}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(Chat)