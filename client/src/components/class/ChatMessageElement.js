import React from 'react'


export default ({ message }) =>
    <p className="message">
      [<strong>{message[0]}</strong>]:  {message[1]}
    </p>