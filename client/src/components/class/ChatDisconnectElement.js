import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Button, InputGroup } from "react-bootstrap";

class ChatDisconnectElement extends Component {
    static propTypes = {
      onDisconnect: PropTypes.func.isRequired,
    }
  
    render() {
      return (
        <form
          action="."
          onSubmit={e => {
            e.preventDefault()
            this.props.onDisconnect()
          }}>
            <InputGroup className="flex">
                <Button block type="submit">Disconnect</Button>
            </InputGroup>
        </form>
      )
    }
  }
  
  export default ChatDisconnectElement