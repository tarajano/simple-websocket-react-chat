import React, { useState } from "react";
import { useHistory } from 'react-router-dom'
import { Button, FormGroup, FormControl } from "react-bootstrap";
import "./Connect.css";

export default function Connect() {
  const [username, setUsername] = useState("");
  const history = useHistory();

  function validateForm() {
    return username.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    history.push({pathname:`/chat/${username}`});
  }

  return (
    <div className="Connect">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="username">
          <FormControl
            autoFocus
            type="text"
            placeholder="Username"
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
        </FormGroup>
        <Button block disabled={!validateForm()} type="submit">
          Connect
        </Button>
      </form>
    </div>
  );
}
