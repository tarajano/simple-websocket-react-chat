# Simple Go-React-Websocket Chat (Client-side)

The client-side of the application has been developed in ReactJS.
It uses multiple modules that need to be present in order to run the client-side (*they are specified in the RESOURCES section*).

## How to run the application

Running the client-side is pretty straight forward. Simply execute the following commands.

* **`npm i`**

    This command downloads the project's dependencies in the package.json file.
    Once it finishes, move to the next command.

* **`npm start`**

    This command runs the app in the development mode.
    Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Resources 

The client-side uses the following modules:

* axios [^0.20.0]
* react [^16.13.1]
* react-bootstrap [^1.3.0]
* react-dom [16.13.1]
* react-router-dom [5.2.0]
* react-scripts [3.4.3]

## Improvements

* It is possible to make the application more verbose in order to improve error handling.
* The application can use .env files to specify the server URL in a more flexible way.
