package main

import (
	"log"
	"net/http"
	"strings"
	"sync"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/rs/cors"
)

// WSData holds the relevant information about a message received from a user
type WSData struct {
	Source  string `json:"source"`
	Type    string `json:"type"`
	Content string `json:"content,omitempty"`
}

// UserConnection holds the relevant information about the user connection
type UserConnection struct {
	Username   string
	Connection *websocket.Conn
}

// RegisteredConnections holds the relevant information about all the connections and a mutex to synchronize access to it
type RegisteredConnections struct {
	Mu          sync.Mutex
	Connections []*UserConnection
}

// Declaration of the upgrader to upgrade the connection from HTTP to WS
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Declaration of an array of connections to hold the information about the connected users
var connections RegisteredConnections

// Declare channels to manage received messages and connections
var incomingMessage = make(chan WSData)
var incomingConnections = make(chan *UserConnection)

func main() {

	// Spawn goroutine to handle events
	go handleEvents(incomingMessage, incomingConnections)

	// Declare the gorilla mux router to handle specific endpoints
	router := mux.NewRouter()
	router.HandleFunc("/chat/{username}", chatHandler)

	// Definition of a variable to manage CORS
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	handler := c.Handler(router)
	log.Fatal(http.ListenAndServe(":8080", handler))
}

// handleEvents checks the possible server-side events and execute code depending on the event.
func handleEvents(incomingMessage chan WSData, incomingConnections chan *UserConnection) {
	for {
		select {
		case message := <-incomingMessage:
			broadcastMessage(message)
		case connection := <-incomingConnections:
			registerConnection(connection)
			broadcastConnectedUsers()
		}
	}
}

// broadcastMessage broadcasts data to connected users.
func broadcastMessage(data WSData) {
	connections.Mu.Lock()
	for _, connection := range connections.Connections {
		connection.Connection.WriteJSON(data)
	}
	connections.Mu.Unlock()
}

// registerConnection adds a new connection to the list of current connections.
func registerConnection(connection *UserConnection) {
	connections.Mu.Lock()
	connections.Connections = append(connections.Connections, connection)
	connections.Mu.Unlock()
}

// unregisterConnection remove an existing connection from the list of current connections.
func unregisterConnection(connection *websocket.Conn) {
	index := 0
	for index = 0; index < len(connections.Connections); index++ {
		if connection == connections.Connections[index].Connection {
			break
		}
	}
	if index < len(connections.Connections) {
		connections.Mu.Lock()
		connections.Connections = append(connections.Connections[:index], connections.Connections[index+1:]...)
		connections.Mu.Unlock()
	}
}

// broadcastConnectedUsers broadcasts the list of connected users to be display in the client side.
func broadcastConnectedUsers() {
	connections.Mu.Lock()
	users := getConnectedUsers()
	for _, connection := range connections.Connections {
		connection.Connection.WriteJSON(map[string]interface{}{"type": "userslist", "users": users})
	}
	connections.Mu.Unlock()
}

// getConnectedUsers returns a list of the usernames connected to the server.
func getConnectedUsers() (users []string) {
	for _, userConnection := range connections.Connections {
		users = append(users, userConnection.Username)
	}
	return
}

// isUsernameAvailable checks whether a username is already taken or not.
func isUsernameAvailable(username string) bool {
	isAvailable := true
	username = strings.ReplaceAll(username, " ", "")

	if len(username) > 0 {
		for _, connection := range connections.Connections {
			if username == connection.Username {
				isAvailable = false
				break
			}
		}
	} else {
		isAvailable = false
	}
	return isAvailable
}

// chatHandler is the method associated to the /chat route, that handles the reception of data and the registration of a connection/disconnection of an user.
func chatHandler(writer http.ResponseWriter, request *http.Request) {

	// Extract the username from the request and check if it is already taken
	username := mux.Vars(request)["username"]
	if isUsernameAvailable(username) == false {
		http.Error(writer, "USERNAME_NOT_AVAILABLE", http.StatusForbidden)
		return
	}

	// Upgrade the connection to WS
	conn, err := upgrader.Upgrade(writer, request, nil)
	if err != nil {
		log.Fatal(err)
	}

	// Specify the course of actions when a user disconnects from the service
	defer func() {
		unregisterConnection(conn)
		broadcastConnectedUsers()
		conn.Close()
	}()

	// Register the connection
	incomingConnections <- &UserConnection{username, conn}

	// Infinite loop that listens to receive data
	for {
		jsonData := WSData{}
		if err := conn.ReadJSON(&jsonData); err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				break
			} else {
				log.Fatal(err)
			}
		}

		switch jsonData.Type {
		case "message":
			incomingMessage <- jsonData
		default:
			// it is possible to add different types of data to handle. By now, it just accept messages
		}
	}
}
