# Simple Go-React-Websocket Chat (Client-side)

The server-side of the application has been developed in Go.
It uses multiple libraries that need to be present in order to run the server-side (*they are specified in the RESOURCES section*).

## How to run the application

Running the server-side is pretty straight forward.
First, you need to make sure to have the required libraries.
If it is not the case you can execute:

* **`go get {resource}`**

    This command will download the specified resource for you, making it available for the application to use.

Once the required libraries are present in the system, you could simply run:
    
* **`go run main.go`**

    This command runs the app in the address [http://localhost:8080](http://localhost:8080).
    
## Endpoints

At the moment, there is only one endpoint that could be used:

* **`http://localhost:8080/chat/{username}`**

    A call to this endpoint will try to establish a connection using Websocket technology, trying to upgrade the HTTP/HTTPS connection to it.
    If any error occurs or the username selected is already taken, the application will redirect the user to the connection page.

## Resources 

The client-side uses the following modules:

* "github.com/gorilla/mux" gorilla/mux
* "github.com/gorilla/websocket" Gorilla WebSocket
* "github.com/rs/cors" Go CORS handler

## Improvements

* It is possible to make the application more verbose in order to improve error handling.
* The application uses the Websocket protocol (ws), but it could be upgraded to a more secure protocol using SSL (wss).
* The application can be extended to handle more events.
