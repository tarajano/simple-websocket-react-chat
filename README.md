# Simple Go-React-Websocket Chat

This application was created to serve as a POC to put together different technologies, mainly Go, ReactJS and Websockets.
It is a simple webchat that features a server-side made in Go, a client-side made in ReactJS and websockets as the communication protocol.

## How to run the application

To run the application, you just need to go into the server and client folders and follow the instructions in the README.md file.
